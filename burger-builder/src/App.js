import React,{Component} from 'react';
import LayoutComponent from './Components/Layout/Layout';
import BurgerBuilder from '../src/Containers/BurgerBuilder/BurgerBuilder';

class App extends Component {
  render(){
    return (
      <div className="App">
        <LayoutComponent > 
          <BurgerBuilder></BurgerBuilder>
        </LayoutComponent>
      </div>
    );
  }
    
  
}

export default App;
