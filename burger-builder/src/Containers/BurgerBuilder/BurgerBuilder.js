import React, {Component} from 'react';
import Aux from '../../hoc/Aux';
import Burger from '../../Components/Burger/Burger';
import '../../index.css';
import axios from 'axios';

class BurgerBuilder extends Component {

    state =  {
        ingradients: {            
            salad: 1,
            cheese: 1,
            meat: 1,
            bacon: 1
        }
    }

    componentDidMount() {
        axios.get('http://jsonplaceholder.typicode.com/posts')
            .then(response => {
                console.log(response);
            })
            .catch(
                console.log('call is success')
            );
    }

    render() {
        return(
            <Aux>
                <div>
                    <Burger ingradients= {this.state.ingradients}/>
                </div>
            </Aux>
        );
    }
}

export default BurgerBuilder;