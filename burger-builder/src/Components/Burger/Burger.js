import React from 'react';
import './Burger.css';
import './BurgerIngradient/BurgerIngradient.css';
import BurgerIngradient from './BurgerIngradient/BurgerIngradient';

const burger = (props) => {

    const transaformedBurgerIngradients = Object.keys(props.ingradients)
        .map(igKey =>
            { return [...Array(props.ingradients[igKey])]
                .map((_,i) => {
                    return <BurgerIngradient key={igKey+i} type={igKey}/>
                } ); } 
        );

    console.log(transaformedBurgerIngradients);

    return (
        <div className='Burger'> 
            <div ><BurgerIngradient type="bread-top"/></div>            
            {transaformedBurgerIngradients}
            <div ><BurgerIngradient type="bread-bottom"/></div>
        </div>
    );
};

export default burger;